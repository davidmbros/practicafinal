package videoClub;

public class ListaVideoJuegos {
	
	//Atributos
		private NodoVideoJuegos cabecera;
		private NodoVideoJuegos ultimo;
		private int cont;  //cuenta los elementos de la lista
		private static int maximo=1000;
		//Constructores
		
		protected ListaVideoJuegos(){
			cabecera=null;
			ultimo=null;
			cont=0;
		}
		
	//Metodos y funciones
		
		//Funcion que devuelve si la lista esta vacia o no
		
		protected boolean EsVacia(){
			return (cabecera==null)?true:false;
		}
		
		//Lista Llena
		
		protected boolean EsLlena(){
			return (cont==maximo)?true:false;
			
		}
		
		
		//Recorer la lista
		
		protected void RecorrerLista(){
		
			NodoVideoJuegos aux=cabecera;
			if (EsVacia()){
				System.out.println("La lista esta vacia");
			}else{
				while (aux!=null){
					System.out.println(aux.getjuego().MostrarVideoJuegos());
					System.out.println("\tCopias en el almac�n: "+aux.GetCopias());
					aux=aux.sig;
				}
			}
		}
		

		protected void InsertarOrdenada(videojuego juego){
			
			if (EsLlena()){
				System.out.println("La lista esta llena");
			}else {
				NodoVideoJuegos nuevo=new NodoVideoJuegos(juego);
				if (EsVacia()){
					cabecera=nuevo;
					ultimo=nuevo;
				}else {
					if (cabecera.getjuego().GetTitulo().compareToIgnoreCase(juego.GetTitulo())>0){
						nuevo.sig=cabecera;
						cabecera.ant=nuevo;
						cabecera=nuevo;
					}else {//Esta en el medio o el el ultimo						
						NodoVideoJuegos aux=cabecera;
						boolean insertado=false;
						while ((aux.sig!=null) && (!insertado)){
							if (aux.sig.getjuego().GetTitulo().compareToIgnoreCase(juego.GetTitulo())<0)
								aux=aux.sig;
							else {//esta en el medio, lo insertamos								
								insertado=true;
								nuevo.sig=aux.sig;
								nuevo.ant=aux;
								aux.sig=nuevo;
								nuevo.sig.ant=aux;
							}
						}//Si no lo ha insertado, es el ultimo						
						if(!insertado){
							aux.sig=nuevo;
							nuevo.ant=aux;
							ultimo=nuevo;
						}
					}
				}
				cont++;
			}
		}
		
		
		
		//Borramos un nodoSocio, pasamos el dni para borrar el nodo
		
		protected void BorrarSolicitado(String titulo){
			NodoVideoJuegos aux=cabecera;
				if(EsVacia()){
					System.out.println("La lista esta vac�a");
				}else {//comprobamos si borramos la cabecera					
					if (cabecera.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0){
						if(cont==1){
							cabecera=null;
						}
						else{//Borramos la cabecera						
						cabecera=cabecera.sig;
						cabecera.ant=null;
						}
					}else{//No es la cabecera, recorremos hasta encontrarlo, si existe				
						boolean encontrado=false;
						while ((aux.sig!=null)&&(!encontrado)){
							if (aux.sig.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0)
								encontrado=true;
						else {
							aux=aux.sig;
						}
					}//Si lo encontramos entonces esta o en en medio o al final
					if (encontrado){
						if(aux.sig!=ultimo){
							aux.sig=aux.sig.sig;
							aux.sig.ant=aux;
						}else{
							aux.sig=null;
							ultimo.ant=null;
							ultimo=aux;
						}
						cont--;
					}
					else System.out.println("El dato a buscar no existe");
				}
				System.gc();
			}
		}
		

		//Buscar un socio en la lista, esta busqueda se hace pasando el DNI del socio 
		
		protected String BuscarSolicitado(String titulo){
			
			if(EsVacia()){
				return null;
			}else {
				NodoVideoJuegos aux=cabecera;
				while (aux!=null){
					if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0){
						return aux.getjuego().MostrarVideoJuegos();
					}else if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		//Existe un socio en la lista, esta busqueda se hace pasando el DNI del socio
		
		protected boolean Existe(String titulo){
			
			if(EsVacia()){
				return false;
			}else {
				NodoVideoJuegos aux=cabecera;
				while (aux!=null){
					if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0){
						return true;
					}else if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)<0)
					aux=aux.sig;
				else return false;
				}
				return false;		
			}
		}
		
		//Buscar un socio en la lista, esta busqueda se hace pasando el DNI del socio 
		
		protected boolean AniadirCopia(String titulo){
					
					if(EsVacia()){
						return false;
					}else {
						NodoVideoJuegos aux=cabecera;
						while (aux!=null){
							if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0){
								int copias = aux.GetCopias();
								aux.SetCopias(copias+1);;
								return true;
							}else if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)!=0)
								aux=aux.sig;
							else return false;
						}
						return false;		
					}
				}

		// Devuelve el nodo en el que esta el videojuego a buscar por el titulo
		
		protected NodoVideoJuegos ObtenerJuego(String titulo){
			
			if(EsVacia()){
				return null;
			}else {
				NodoVideoJuegos aux=cabecera;
				while (aux!=null){
					if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)==0){
						return aux;
					}else if (aux.getjuego().GetTitulo().compareToIgnoreCase(titulo)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
}

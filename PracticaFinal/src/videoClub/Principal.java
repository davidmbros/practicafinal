package videoClub;

import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {
	
	/* Trabajo realizado por David Manzano Bravo
	 * Asignatura: Programaci�n
	 * Tarea de listas dobles enlazadas con Objetos
	 * Videoclub de Videojuegos
	 */
	
	public static ListaSocios lista=new ListaSocios();
	public static ListaVideoJuegos listaV=new ListaVideoJuegos();
	public static ListaAlquileres listaA=new ListaAlquileres();
	
	public static void Menu(){
		Scanner sc=new Scanner(System.in);
		
		int opc=0;
		do{
			try{
				System.out.print("\n");	
				System.out.print("\n\n\t\tVIDEOCLUB ALQUILA TUS JUEGOS");
				System.out.print("\n\t1.- Cliente");
				System.out.print("\n\t2.- Juegos");
				System.out.print("\n\t3.- Alquilar");
				System.out.print("\n\t0.- Salir");
				System.out.print("\n\tIntroduzca una opci�n: ");
				opc=sc.nextInt();
				
				switch (opc){
					case 1:
						MenuCliente();
						break;
					case 2:
						MenuVideoJuego();
						break;
					case 3:
						MenuAlquiler();
						break;
				}			
			}catch(InputMismatchException e){
				opc=0;
				sc.nextLine();
				System.out.println("Error!");
				Menu();
			}
		}while(opc!=0);
		System.out.println("Saliendo...");
	}
	
	public static void MenuCliente(){
		Scanner sc=new Scanner(System.in);
		String Dni;
		int opc=0;
		
		//Creamos el menu de opciones
		do {
			//Try catch para evitar que el programa termine si hay un error
		try{
			System.out.print("\n");	
			System.out.print("\n\n\t\tSOCIOS");
			System.out.print("\n\t1.- Insertar nuevo cliente");
			System.out.print("\n\t2.- Listar clientes");
			System.out.print("\n\t3.- Buscar un cliente");
			System.out.print("\n\t4.- Borrar cliente");
			System.out.print("\n\t0.- Menu Principal");
			System.out.print("\n\tIntroduzca una opci�n: ");
			opc=sc.nextInt();
			
			switch (opc){
				case 1: //Insertar nuevo cliente
					System.out.println("Indique el DNI del cliente a dar de alta (Ejemplo: 12345678A):");
					Dni=sc.next();
					if (lista.Existe(Dni)==false){
						System.out.println("Introduzca el Nombre (Ejemplo: David):");
						String nombre=sc.next();
						sc.nextLine(); 
						System.out.println("Introduzca los Apellidos (Ejemplo: Manzano Bravo):");
						String apellidos=sc.nextLine();
						System.out.println("Introduzca el numero de telefono (Ejemplo: 666555444):");
						int telefono=sc.nextInt();
						socios cliente = new socios(nombre,apellidos,Dni,telefono);
						lista.InsertarOrdenada(cliente);
						System.out.println("El cliente a sido agregado");
					}else{
						System.out.println("El socio ya existe");
						}
					break;
				case 2: //Listar clientes
					if (lista.EsVacia()==false){
					System.out.print("\n  Apellidos\tNombre\t Telefono\t  Dni\n");
					lista.RecorrerLista();
					}else{
						System.out.println("La lista esta vacia");
					}
					break;
				case 3: //Buscar un cliente por DNI
					System.out.println("Indique el DNI del cliente a buscar:");
					Dni=sc.next();
					if (lista.BuscarSolicitado(Dni)!=null){
						System.out.print("\n  Apellidos\tNombre\t Telefono\t  Dni\n");
						System.out.print(lista.BuscarSolicitado(Dni));
					}else {
						System.out.println("El cliente no existe");
					}
					break;
				case 4: //Borrar cliente por DNI
					System.out.println("Indique el DNI del cliente a borrar:");
					Dni=sc.next();
					if (lista.BuscarSolicitado(Dni)!=null){
						lista.BorrarSolicitado(Dni);
						System.out.println("La socio ha sido borrado");
					}	else{
						System.out.println("La socio no existe");
					}
				break;

			}
				System.out.println("\n");
		
		}catch(InputMismatchException e){
				opc=0;
				sc.nextLine();
				System.out.println("Error!");
				Menu();
		}
		}while(opc!= 0);
		
		System.out.println("Volviendo al menu principal...");
		
		
	}
	
	public static void MenuVideoJuego(){
		
		String Titulo;
		int opc=0;
		Scanner sc=new Scanner(System.in);
		//Creamos el menu de opciones
		do {
			//Try catch para evitar que el programa termine si hay un error
		try{
			System.out.print("\n");	
			System.out.print("\n\n\t\tVIDEOJUEGOS");
			System.out.print("\n\t1.- Insertar nuevo Videojuego");
			System.out.print("\n\t2.- Listar Juegos");
			System.out.print("\n\t3.- Buscar un Juego en almacen");
			System.out.print("\n\t4.- Borrar Juego");
			System.out.print("\n\t0.- Menu Principal");
			System.out.print("\n\tIntroduzca una opci�n: ");
			opc=sc.nextInt();
			sc.nextLine();
			switch (opc){
				case 1: //Insertar nuevo Videojuego
					System.out.println("Indique el titulo del Juego a dar de alta (Ejemplo: FIFA 20):");
					Titulo=sc.nextLine();
					
					//Compara el titulo con los de la lista, si existe a�ade una copia si no
					//existe se tiene que crear
					
					if (listaV.Existe(Titulo)!=true){
						System.out.println("Introduzca el Categoria (Ejemplo: Deportes):");
						String Categoria=sc.next();
						sc.nextLine(); 
						System.out.println("Introduzca el a�o del juego (Ejemplo: 1990):");
						int anio=sc.nextInt();
						System.out.println("Introduzca el precio de compra del Juego (Ejemplo: 20.90):");
						Double precioCompra=sc.nextDouble();
						videojuego juego =new videojuego(Titulo,Categoria,anio,precioCompra);
						listaV.InsertarOrdenada(juego);
						System.out.print("\nEl juego a sido agregado");
					}else{
						
						int copias = listaV.ObtenerJuego(Titulo).GetCopias();
						listaV.ObtenerJuego(Titulo).SetCopias(copias+1);
						System.out.println("\nEl juego ya esta en el inventario");
						System.out.print("Se ha a�adido una copia nueva.\nHay en el almacen ahora: "+(copias+1)+" juegos de: "+Titulo);
						
					}
					break;
				case 2: //Listar todos los Juegos de la lista
					if (listaV.EsVacia()==false){
						System.out.print("\n\n\t\tLISTA DE JUEGOS\n");
						listaV.RecorrerLista();
					}else{
						System.out.println("La lista esta vacia");
					}
					break;
				case 3: //Buscar un Juego en la lista si existe le muestra si no te dice que no existe
					System.out.print("Indique el titulo del videojuego a buscar:");
					Titulo=sc.nextLine();
					if (listaV.BuscarSolicitado(Titulo)!=null){
						System.out.print("\n"+listaV.BuscarSolicitado(Titulo));
						int copias = listaV.ObtenerJuego(Titulo).GetCopias();
						System.out.print("\n\tNumero de copias en el almacen: "+(copias));
					}else {
						System.out.println("El juego no existe");
					}
					break;
				case 4: //Borrar un Juego en la lista si existe le muestra si no te dice que no existe
					System.out.println("Indique el titulo del videojuego a borrar:");
					Titulo=sc.nextLine();
					
					if (listaV.BuscarSolicitado(Titulo)!=null){
						listaV.BorrarSolicitado(Titulo);
						System.out.println("El juego ha sido borrado");
					}	else{
						System.out.println("La juego no existe");
					}
					break;
			}
			
				System.out.println("\n");
		
		}catch(InputMismatchException e){
				opc=0;
				sc.nextLine();
				System.out.println("Error!");
				Menu();
		}
		}while(opc != 0);
		
		System.out.println("Volviendo al menu principal...");
				
	}
	
	public static void MenuAlquiler(){
		
		
		Scanner sc=new Scanner(System.in);
		String Dni;
		int opc=0;
		
		//Creamos el menu de opciones
		do {
			//Try catch para evitar que el programa termine si hay un error
		try{
			System.out.print("\n");	
			System.out.print("\n\n\t\tALQUILAR JUEGO");
			System.out.print("\n\t1.- Insertar nuevo alquiler");
			System.out.print("\n\t2.- Listar alquileres de cliente");
			System.out.print("\n\t3.- Devolver Juego");
			System.out.print("\n\t0.- Menu Principal");
			System.out.print("\n\tIntroduzca una opci�n: ");
			opc=sc.nextInt();
			sc.nextLine();
			switch (opc){
				case 1: //Insertar nuevo alquiler
					System.out.println("Indique el DNI del cliente:");
					Dni=sc.nextLine();
					if (lista.Existe(Dni)!=false){			
					
					System.out.println("Indique el titulo del videojuego a alquilar:");
					String Titulojuego=sc.nextLine();
					
						if (listaV.BuscarSolicitado(Titulojuego)!=null){
							NodoVideoJuegos juego = listaV.ObtenerJuego(Titulojuego);
							
								if (juego.GetCopias()!=0){
									
									//    RUBEN!!!!
									//si que funcion al final maxalquilerdia(), no estaba haciendo bien 
									//la lamada a la lista alquiler del cliente
									
									if(lista.ObtenerCliente(Dni).getAlquiler().maxalquilerdia()==true){
									//inserto el alquiler y decremento una copia que luego al hacer la 
										//devoluci�n se incrementara
									lista.ObtenerCliente(Dni).getAlquiler().InsertarAlquiler(Titulojuego);
									juego.SetCopias(juego.GetCopias()-1);
									
									System.out.println("\nEl juego "+Titulojuego+" ha sido alquilado.");
								}else{
									System.out.println("No puedes alquilar mas juegos por hoy");
								}
									}
								else{
									System.out.println("No hay copias disponibles");
								}
						}
						else
							{
							System.out.println("El Juego no existe o has introducido mal el titulo");
							}
						}
					else
						{
						System.out.println("El socio no existe");
						}
					break;
				case 2: //Listar todos los alquileres del cliente, y te dice cual tiene alquilados y cual no.
					System.out.println("Indique el DNI del cliente para ver sus juegos en alquiler:");
					Dni=sc.nextLine();
					if (lista.Existe(Dni)!=false){
						System.out.println("\nLista de Juegos alquilados de: "+ lista.ObtenerCliente(Dni).getcliente().GetNombre()+
								" "+ lista.ObtenerCliente(Dni).getcliente().GetApellidos());
						lista.ObtenerCliente(Dni).getAlquiler().RecorrerLista();	
					}

					break;
				case 3: //Devolver Juego y el precio si tiene recargo o no
					System.out.println("Indique el DNI del cliente:");
					Dni=sc.nextLine();
					if (lista.Existe(Dni)!=false){			
						
						if (lista.ObtenerCliente(Dni).getAlquiler().EsVacia()!=true){
							
							System.out.println("Indique el titulo del videojuego a devolver:");
							String Titulojuego=sc.nextLine();
							
							if (listaV.BuscarSolicitado(Titulojuego)!=null){
								NodoVideoJuegos juego = listaV.ObtenerJuego(Titulojuego);
								
								Calendar fechaactual = Calendar.getInstance();
								fechaactual.getTimeInMillis();
								long calculo = (fechaactual.getTimeInMillis() - lista.ObtenerCliente(Dni).getAlquiler().ObtenerAlquiler(Titulojuego).getFecha().getTimeInMillis())/1000;
								if (calculo <= 60 ){ //si el tiempo de alquiler es menor a 60 segundos te pone el precio sin recargo
									double total= (double)calculo*(double)0.05;
									System.out.println("NO HAY RECARGO");
									System.out.println("El importe del alquiler es: "+total);
								}else
								{	//si es mayor el tiempo los primeros 60 seg te los pone 0.05 y el resto con recargo 
									//te calcula el recargo por segundos y lo mult por 0.10
									double totalsinrecag = (double)calculo*(double)0.05;
									long total=calculo-60;
									double totalconrecago = (double)total*(double)0.10;
									System.out.println();
									System.out.println("\n \tDEVOLUCION CON RECARGO DE LA DEVOLUCION DE "+Titulojuego);
									System.out.println("\tTiempo que lleva el Juego alquilado: "+calculo);
									System.out.printf("\tEl importe del alquiler es: %.2f",(totalsinrecag+totalconrecago));
								}
								//Tenia puesto que al devolver el juego se borrara el nodo con: 
								//lista.ObtenerCliente(Dni).getAlquiler().DevolverJuego(Titulojuego);
								//Pero como dijiste que se tenian que poner como devuelto, pues cambi� he hice una funcion 
								//boolean para poner alquilada de false a true.
								//que si te devuelve true esk esta devuelta
								//y vuelvo a introducir la copia devuelta
								
								if (lista.ObtenerCliente(Dni).getAlquiler().DevolucionSinBorrar(Titulojuego)==true){
									juego.SetCopias(juego.GetCopias()+1);
									System.out.println("\tDEVOLUCION REALIZADA");
								}	
							}
							else
							{
							System.out.println("El Juego no existe o has introducido mal el titulo");
							}		
						}else
							{
							System.out.println("El socio no tiene alquileres");
							}	
					}else
						{
						System.out.println("El socio no existe");
						}
					
					break;	
			}
				System.out.println("\n");
		
		}catch(InputMismatchException e){
				opc=0;
				sc.nextLine();
				System.out.println("Error!");
				Menu();
		}
		}while(opc != 0);
		
		System.out.println("Volviendo al menu principal...");
		
	}
	

	public static void main(String[] args) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		
		/*
		socios cliente1 =new socios("David","Manzano Bravo","12345678V",666555444);
		lista.InsertarOrdenada(cliente1);
		socios cliente2 =new socios("Rub�n","Alvarez Perez","12345678B",666555444);
		lista.InsertarOrdenada(cliente2);
		socios cliente3 =new socios("Osvaldo","Zarza Bravo","12345678C",666555444);
		lista.InsertarOrdenada(cliente3);
		socios cliente4 =new socios("Juan","Branda Bravo","12345678VZ",666555444);
		lista.InsertarOrdenada(cliente4);
		
		videojuego juego1 = new videojuego("Resident Evil 7: Biohazard", "Survival horror", 2017, 20.00);
		listaV.InsertarOrdenada(juego1);
		videojuego juego2 = new videojuego("FIFA 17", "Deportes", 2016, 20.00);
		listaV.InsertarOrdenada(juego2);
		listaV.AniadirCopia("FIFA 17");
		listaV.AniadirCopia("FIFA 17");
		listaV.AniadirCopia("FIFA 17");
		listaV.AniadirCopia("FIFA 17");
		videojuego juego3 = new videojuego("Battlefield 1", "Accion", 2016, 20.00);
		listaV.InsertarOrdenada(juego3);
		*/
		
		Menu();

		
	}
}

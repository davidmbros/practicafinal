package videoClub;

import java.util.Calendar;

public class NodoAlquiler {
	
	//Atributos
	
		private String Titulojuego;
		private Calendar fechaalquiler;	
		private Boolean alquilada;
		protected NodoAlquiler sig;
		protected NodoAlquiler ant;		
		
	//Constructor
		public NodoAlquiler(String Titulojuego){
			this.Titulojuego=Titulojuego;
			fechaalquiler = Calendar.getInstance();
			this.fechaalquiler.getTimeInMillis();
			this.alquilada=true;
			sig=null;
			ant=null;
		}
		
		//Metodos get y set
		
		protected String getalquiler(){
			return Titulojuego;
		}
		
		protected void setalquiler(String Titulojuego){
			this.Titulojuego=Titulojuego;
		}
		
		protected Calendar getFecha() {
			return fechaalquiler;
		}

		protected Boolean getAlquilada() {
			return alquilada;
		}

		protected void setAlquilada(Boolean alquilada) {
			this.alquilada = alquilada;
		}
		
		protected String mostraralquiler(){
			return Titulojuego;
		}

}

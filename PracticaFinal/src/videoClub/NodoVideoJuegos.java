package videoClub;

public class NodoVideoJuegos {
		
	//Atributos
		private videojuego juego;
		protected NodoVideoJuegos sig;
		protected NodoVideoJuegos ant;
		private int copiaJuego;
		
		
		
	//Constructor
		protected NodoVideoJuegos(videojuego juego){
			this.juego=juego;
			sig=null;
			ant=null;
			copiaJuego=1;
			
		}
		
	//Metodos get y set
		
		protected videojuego getjuego(){
			return this.juego;
		}
		
		protected void setjuego(videojuego juego){
			this.juego=juego;
		}
		
		protected int GetCopias() {
			return copiaJuego;
		}

		protected void SetCopias(int copiaJuego) {
			this.copiaJuego = copiaJuego;
		}
}


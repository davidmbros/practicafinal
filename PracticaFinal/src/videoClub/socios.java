package videoClub;

public class socios {
	
	//Atributos
		private String nombre;
		private String apellidos;
		private String dni;
		private int telefono;
	
	//Constructores
	
		protected socios(){
			nombre="";
			apellidos="";
			dni="";
			telefono=0;
		}
		
		protected socios(String nombre, String apellidos,String dni,int telefono){
			this.nombre=nombre;
			this.apellidos=apellidos;
			this.dni=dni;
			this.telefono=telefono;
		}
	
	
	//Getter y Setter
	
		protected void setNombre(String nombre){
			this.nombre=nombre;
		}
		
		protected void setApellidos(String apellidos){
			this.apellidos=apellidos;
		}
		
		protected void setDni(String dni){
			this.dni=dni;
		}
		
		protected void setTelefono(int telefono){
			this.telefono=telefono;
		}
		
		protected String GetNombre(){
			return nombre;
		}
		
		protected String GetApellidos(){
			return apellidos;
		}
	
		
		protected String GetDni(){
			return dni;
		}
		
		protected int telefono(){
			return telefono;
		}
		
		protected String MostrarSocio(){
			
			return "\n"+apellidos+"\t"+nombre+"\t"+telefono+"\t"+dni;
		}
	
}


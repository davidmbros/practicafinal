package videoClub;

public class ListaSocios {

	//Atributos
	
		private NodoSocios cabecera;
		private NodoSocios ultimo;
		private int cont;  //cuenta los elementos de la lista
		private static int maximo=150;
		
	//Constructores
	
		protected ListaSocios(){
			cabecera=null;
			ultimo=null;
			cont=0;
		}
	
	//Metodos y funciones
	
		//Funcion que devuelve si la lista esta vacia o no
		protected boolean EsVacia(){
			return (cabecera==null)?true:false;
		}
		
		//Lista Llena
		
		protected boolean EsLlena(){
			return (cont==maximo)?true:false;
		}
		
		//Recorer la lista
		
		protected void RecorrerLista(){
			//Variable que recorre la lista
			NodoSocios aux=cabecera;
			if (EsVacia()){
				System.out.println("La lista esta vacia");
			}else{
				while (aux!=null){
					System.out.println(aux.getcliente().MostrarSocio());
					aux=aux.sig;
				}
			}
		}
		
		protected void InsertarOrdenada(socios cliente){
			
			if (EsLlena()){
				System.out.println("La lista esta llena");
			}else {
				NodoSocios nuevo=new NodoSocios(cliente);
				if (EsVacia()){
					cabecera=nuevo;
					ultimo=nuevo;
				}else {
					if (cabecera.getcliente().GetApellidos().compareToIgnoreCase(cliente.GetApellidos())>0){
						nuevo.sig=cabecera;
						cabecera.ant=nuevo;
						cabecera=nuevo;
					}else {// insertar en el medio o el el ultimo					
						NodoSocios aux=cabecera;
						boolean insertado=false;
						while ((aux.sig!=null) && (!insertado)){
							if (aux.sig.getcliente().GetApellidos().compareToIgnoreCase(cliente.GetApellidos())<0)
								aux=aux.sig;
							else {//si esta en el medio, lo insertamos							
								insertado=true;
								nuevo.sig=aux.sig;
								nuevo.ant=aux;
								aux.sig=nuevo;
								nuevo.sig.ant=aux;
							}
						}//Si no lo ha insertado, entonces la insercion se hara en el ultimo					
						if(!insertado){
							aux.sig=nuevo;
							nuevo.ant=aux;
							ultimo=nuevo;
						}
					}
				}
				cont++;
			}
		}
			
		//Borramos un nodoSocio, pasamos el dni para borrar el nodo
		
		protected void BorrarSolicitado(String Dni){
			NodoSocios aux=cabecera;
				if(EsVacia()){
					System.out.println("La lista esta vac�a");
				}else {//comprobamos si borramos la cabecera
					if (cabecera.getcliente().GetDni().compareToIgnoreCase(Dni)==0){
						if(cont==1){
							cabecera=null;
						}
						else{//Borramos la cabecera
						cabecera=cabecera.sig;
						cabecera.ant=null;
						}
					}else{//No es la cabecera, recorremos hasta encontrarlo, si existe			
						boolean encontrado=false;
						while ((aux.sig!=null)&&(!encontrado)){
							if (aux.sig.getcliente().GetDni().compareToIgnoreCase(Dni)==0)
								encontrado=true;
						else {
							aux=aux.sig;
							
						}
					}//Lo encontramos, puede estar en en medio o al final				
					if (encontrado){
						if(aux.sig!=ultimo){
							aux.sig=aux.sig.sig;
							aux.sig.ant=aux;
						}else{
							aux.sig=null;
							ultimo.ant=null;
							ultimo=aux;
						}
						cont--;
					}
					else System.out.println("El dato a buscar no existe");
				}
				System.gc();
			}
		}
		
		//Buscar un socio en la lista, esta busqueda se hace pasando el DNI del socio 
		
		protected String BuscarSolicitado(String Dni){
			
			if(EsVacia()){
				return null;
			}else {
				NodoSocios aux=cabecera;
				while (aux!=null){
					if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)==0){
						return aux.getcliente().MostrarSocio();
					}else if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		//Existe un socio en la lista, esta busqueda se hace pasando el DNI del socio
		
		protected boolean Existe(String Dni){
			
			if(EsVacia()){
				return false;
			}else {
				NodoSocios aux=cabecera;
				while (aux!=null){
					if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)==0){
						return true;
					}else if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)!=0)
					aux=aux.sig;
				else return false;
				}
				return false;		
			}
		}
		
		protected NodoSocios ObtenerCliente(String Dni){
			
			if(EsVacia()){
				return null;
			}else {
				NodoSocios aux=cabecera;
				while (aux!=null){
					if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)==0){
						return aux;
					}else if (aux.getcliente().GetDni().compareToIgnoreCase(Dni)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		}


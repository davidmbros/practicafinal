package videoClub;

import java.util.Calendar;

public class ListaAlquileres {
	
	//Atributos
	
		private NodoAlquiler cabecera;
		private NodoAlquiler ultimo;
		private int cont;  //cuenta los elementos de la lista
		private static int maximo=100;
		private Calendar fechaactual;
	
	//Constructores
	
		protected ListaAlquileres() {
			
			this.cabecera=null;
			this.ultimo = null;
			this.cont = 0;
		}
	
	//Metodos y funciones
	
		//Funcion que devuelve si la lista esta vacia o no
		protected boolean EsVacia(){
			return (cabecera==null)?true:false;
		}
		//Lista Llena
		protected boolean EsLlena(){
			return (cont==maximo)?true:false;
		}
		
		//Recorer la lista para mostrar los juegos alquilados
		
		protected void RecorrerLista(){
			NodoAlquiler aux=cabecera;
			if (EsVacia()){
				System.out.println("El socio no tiene alquileres");
			}else{
				while (aux!=null){
					fechaactual = Calendar.getInstance();
					this.fechaactual.getTimeInMillis();
					long calculo = (fechaactual.getTimeInMillis() - aux.getFecha().getTimeInMillis())/1000;
					String alqu = null;
						if (aux.getAlquilada()){
							alqu="Si";
						}else alqu="No";
						System.out.println("\nTitulo: "+aux.getalquiler()+"\nTiempo en alquiler: "+calculo+"\nAlquilado: "+alqu);
						aux=aux.sig;
				}
			}
		}
		
		
		//Insertar un juego a alqular
		
		protected void InsertarAlquiler(String Titulojuego){
			
			
			if (EsLlena()){
				System.out.println("La lista esta llena");
			}else {
				NodoAlquiler nuevo=new NodoAlquiler(Titulojuego);
				if (EsVacia()){
					cabecera=nuevo;
					ultimo=nuevo;
				}else {
					if (cabecera.getalquiler().compareToIgnoreCase(Titulojuego)>0){
						nuevo.sig=cabecera;
						cabecera.ant=nuevo;
						cabecera=nuevo;
					}else {		//Esta en el medio o el el ultimo						
						NodoAlquiler aux=cabecera;
						boolean insertado=false;
						while ((aux.sig!=null) && (!insertado)){
							if (aux.sig.getalquiler().compareToIgnoreCase(Titulojuego)<0)
								aux=aux.sig;
							else {//esta en el medio, lo insertamos
								insertado=true;
								nuevo.sig=aux.sig;
								nuevo.ant=aux;
								aux.sig=nuevo;
								nuevo.sig.ant=aux;
							}
						}//Si no lo ha insertado, es el ultimo.
						if(!insertado){
							aux.sig=nuevo;
							nuevo.ant=aux;
							ultimo=nuevo;
						}
					}
				}
				cont++;
			}
		}
	
		//Hacer una devolucion del juego sin borrar el nodo poniendo alquilada a false
		
		protected boolean DevolucionSinBorrar(String Titulojuego){
			
			if(EsVacia()){
				return false;
			}else {
				NodoAlquiler aux=cabecera;
				while (aux!=null){
					if (aux.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						aux.setAlquilada(false);
						return true;
					}else if (aux.getalquiler().compareToIgnoreCase(Titulojuego)!=0)
						aux=aux.sig;
					else return false;
				}
				return false;		
			}
		}
		
		
		
		//Borramos un NodoAlquiler, pasamos el titulo del juego a borrar
		
		protected void DevolverJuego(String Titulojuego){
			NodoAlquiler aux=cabecera;
				if(EsVacia()){
					System.out.println("La lista esta vac�a");
				}else {//comprobamos si borramos la cabecera
					if (cabecera.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						if(cont==1){
							cabecera=null;
						}
						else{//Borramos la cabecera
						cabecera=cabecera.sig;
						cabecera.ant=null;
						}
					}else{//No es la cabecera, recorremos hasta encontrarlo, si existe
						boolean encontrado=false;
						while ((aux.sig!=null)&&(!encontrado)){
							if (aux.sig.getalquiler().compareToIgnoreCase(Titulojuego)==0)
								encontrado=true;
						else {
							aux=aux.sig;
							
						}
					}//Lo encontramos, puede estar en en medio o al final
					if (encontrado){
						if(aux.sig!=ultimo){
							aux.sig=aux.sig.sig;
							aux.sig.ant=aux;
						}else{
							aux.sig=null;
							ultimo.ant=null;
							ultimo=aux;
						}
						cont--;
					}
					else System.out.println("El Juego no esta alqulado");
				}
				System.gc();
			}
		}
	

		//Buscar un Juego en la lista, esta busqueda se hace pasando el Titulo del juego 
		
		protected String BuscarSolicitado(String Titulojuego){
			
			if(EsVacia()){
				return null;
			}else {
				NodoAlquiler aux=cabecera;
				while (aux!=null){
					if (aux.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						return aux.getalquiler();
					}else if (aux.getalquiler().compareToIgnoreCase(Titulojuego)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		//Existe un Juego en la lista, esta busqueda se hace pasando elTitulo del juego 
		
		protected boolean Existe(String Titulojuego){
			
			if(EsVacia()){
				return false;
			}else {
				NodoAlquiler aux=cabecera;
				while (aux!=null){
					if (aux.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						return true;
					}else if (aux.getalquiler().compareToIgnoreCase(Titulojuego)<0)
					aux=aux.sig;
				else return false;
				}
				return false;		
			}
		}
		
		//Buscar un Juego en la lista, esta busqueda se hace pasando el Titulo del juego 
		
		protected NodoAlquiler ObtenerAlquiler(String Titulojuego){
			
			if(EsVacia()){
				return null;
			}else {
				NodoAlquiler aux=cabecera;
				while (aux!=null){
					if (aux.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						return aux;
					}else if (aux.getalquiler().compareToIgnoreCase(Titulojuego)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		protected int getcont(){
			return cont;
		}
		
		//Buscar el nodo del alquiler en la lista, esta busqueda se hace pasando el Titulo del juego 
		
		protected NodoAlquiler ObtenerNodoAlquiler(String Titulojuego){
			
			if(EsVacia()){
				return null;
			}else {
				NodoAlquiler aux=cabecera;
				while (aux!=null){
					if (aux.getalquiler().compareToIgnoreCase(Titulojuego)==0){
						return aux;
					}else if (aux.getalquiler().compareToIgnoreCase(Titulojuego)!=0)
						aux=aux.sig;
					else return null;
				}
				return null;		
			}
		}
		
		//Recorre la lista buscando si hay mas de tres alquileres al dia y te devuelve un booleano
		//para ver si puedes alquilar o no
		protected boolean maxalquilerdia(){
			
			int copiasDia = 0;
			
			NodoAlquiler aux=cabecera;
			if (EsVacia()){
				return true;
			}else{
				while (aux!=null){
					fechaactual = Calendar.getInstance();
					this.fechaactual.getTimeInMillis();
					long fuechaAlqu = aux.getFecha().getTimeInMillis();
					long cantidadDia = (fechaactual.getTimeInMillis() - fuechaAlqu);
					if (aux.getAlquilada()==true){
						if (cantidadDia<=86400000){
								copiasDia++;
								aux=aux.sig;
					}}else
						aux=aux.sig;		
					
				}
				
				if (copiasDia<=3){
					return  true;
				}else return false;
			}

		}
				
}

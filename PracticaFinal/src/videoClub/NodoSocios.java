package videoClub;

public class NodoSocios {
	
	//Atributos
		private socios cliente;
		private ListaAlquileres alquiler;
		protected NodoSocios sig;
		protected NodoSocios ant;
	
	
	//Constructor
		protected NodoSocios(socios cliente){
			this.cliente=cliente;
			sig=null;
			ant=null;
			alquiler=new ListaAlquileres();
		}
	
	//Metodos get y set
	
		protected ListaAlquileres getAlquiler() {
			return alquiler;
		}
	
		protected void setAlquiler(ListaAlquileres alquiler) {
			this.alquiler = alquiler;
		}
	
		protected socios getcliente(){
			return this.cliente;
		}
		
		protected void setcliente(socios cliente){
			this.cliente=cliente;
		}
	

}

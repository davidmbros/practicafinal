package videoClub;

public class videojuego {

	//Atributos

	private String titulo;
	private String categoria;
	private int anio;
	private Double precioCompra;
	
	
	//Constructores
	
	protected videojuego(String titulo, String categoria, int anio, Double precioCompra) {
		
		this.titulo = titulo;
		this.categoria = categoria;
		this.anio = anio;
		this.precioCompra = precioCompra;
		
	}

	//Getter y Setter
	
	protected String GetTitulo() {
		return titulo;
	}

	protected void SetTitulo(String titulo) {
		this.titulo = titulo;
	}

	protected String GetCategoria() {
		return categoria;
	}

	protected void SetCategoria(String categoria) {
		this.categoria = categoria;
	}

	protected int GetFechaCompra() {
		return anio;
	}

	protected void SetFechaCompra(int anio) {
		this.anio = anio;
	}

	protected Double GetPrecioCompra() {
		return precioCompra;
	}

	protected void SetPrecioCompra(Double precioCompra) {
		this.precioCompra = precioCompra;
	}

	
	
	protected String MostrarVideoJuegos(){
		
		return "\n\tTitulo: "+titulo+"\n\tCategoria: "+categoria+"\n\tA�os: "+anio+"\n\tPrecio de compra: "+precioCompra;
	}
	
}
